import {Component, OnInit} from '@angular/core';
import {QuizService} from "../../../service/quiz.service";
import {ToastrService} from "ngx-toastr";
import {ActivatedRoute} from "@angular/router";
import {FormArray, FormBuilder, FormGroup, NgForm} from "@angular/forms";


@Component({
  selector: 'app-question-form',
  templateUrl: './question-form.component.html',
  styleUrls: ['./question-form.component.css']
})


export class QuestionFormComponent implements OnInit {

  dynamicArray: Array<any> = [];
  newDynamic: any = {};

  constructor(private formbuilder:FormBuilder,
              private quizService: QuizService,
              private activeRoute: ActivatedRoute,
              private toastr: ToastrService) {
  }

  ngOnInit(): void {

    this.newDynamic = {name: "", correct: false};
    this.dynamicArray.push(this.newDynamic);

  }

  insertRecord(form: NgForm) {
    this.quizService.createQuestion(form).subscribe((result: any) => {
      console.log(result.id)
      if (result.id > 0) {
        this.toastr.success('Intrebare creata cu success', 'Register');
      }
    }, error => {
      console.log(error.error.message);
      this.toastr.error('Error on save: ' + error.error.message);
    });
  }


  addRow() {
    this.newDynamic = {name: "", correct: false};
    this.dynamicArray.push(this.newDynamic);
    return true;
  }

  deleteRow(index: any) {
    if (this.dynamicArray.length == 1) {
      return false;
    } else {
      this.dynamicArray.splice(index, 1);
      return true;
    }
  }


}
