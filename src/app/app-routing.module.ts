import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StudentComponent} from './components/student/studentList/student.component';
import {ProfessorListComponent} from './components/profesor/professor-list/professor-list.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {StudentFormComponent} from './components/student/student-form/student-form.component';
import {ProfessorFormComponent} from './components/profesor/professor-form/professor-form.component';
import {NewsComponent} from './components/news/news/news.component';
import {QuizComponent} from './components/quiz/quizlist/quiz.component';
import {QuestionFormComponent} from "./components/quiz/question-form/question-form.component";
import {QuestionsComponent} from "./components/questions/questions.component";



const routes: Routes = [
  {path: '', component: DashboardComponent},
  {path: 'profesori', component: ProfessorListComponent},
  {path: 'professor/form/add', component: ProfessorFormComponent},
  {path: 'professor/form/edit/:id', component: ProfessorFormComponent},
  {path: 'students', component: StudentComponent},
  {path: 'student/form/add', component: StudentFormComponent},
  {path: 'student/form/edit/:id', component: StudentFormComponent},
  {path: 'news', component: NewsComponent},
  {path: 'quiz', component: QuizComponent},
  {path: 'quiz/qq', component: QuestionFormComponent},
  {path: 'quiz/form/add', component: QuestionsComponent}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]

})
export class AppRoutingModule {
}
