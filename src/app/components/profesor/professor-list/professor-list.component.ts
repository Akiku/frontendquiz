import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {TeacherService} from '../../../service/teacher.service';

@Component({
  selector: 'app-professor-list',
  templateUrl: './professor-list.component.html',
  styleUrls: ['./professor-list.component.css']
})
export class ProfessorListComponent implements OnInit {

  profesori: any[] = [];
  clickButton = true;
  arrowVerse = false;
  constructor(private router: Router,
              private toastr: ToastrService,
              private professorService: TeacherService) { }

  ngOnInit(): void {
     this.getListOfProfesori();
  }


  // Normal shoudl be like this (no definition is public by default)
  // create a subscription

  getListOfProfesori() {
    this.professorService.getListProfessor().subscribe((s: any) => {
      console.log(s);
      this.profesori = s;
    });
  }

  onRemoveProfessor(id: any){
    this.professorService.deleteProfessor(id).subscribe(() => {
      this.getListOfProfesori();
    });
  }

  editProfessor(id: any) {
    this.router.navigate(['professor/form/edit/'+id]);
  }

  changeStatusProfessor(professor: any){
    this.professorService.changeProfessor(professor).subscribe(() => {
      if(professor.active){
        this.toastr.success("Student activat", "Activation");
      }else {
        this.toastr.warning("Student deactivat", "Deactivation");
      }

      // this.ngOnInit();
    }, error => {
      this.toastr.error("Invalid Activation"+error.error.message);
    })
  }

  moveUp(){
    this.clickButton = false;
    this.arrowVerse = false;
    console.log("arrowVerse = false");
  }

  moveDown(){
    this.clickButton = false;
    this.arrowVerse = true;
    console.log("arrowVerse = true");
  }

  deactivateExpander(){
    this.clickButton=true;
  }
}
