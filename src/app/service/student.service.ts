import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  // domain can be taken from env variable list
  private baseUrl = 'http://localhost:8081/api/';

  constructor(private http: HttpClient) { }


  getListStudent(): Observable<any> {
    const reqHeader = new HttpHeaders({'Content-Type': 'application/json', 'No-Auth': 'True'});
    return this.http.get(this.baseUrl+'students', {headers: reqHeader});
  }

  getStudent(id: any): Observable<any> {
    const reqHeader = new HttpHeaders({'Content-Type': 'application/json', 'No-Auth': 'True'});
    return this.http.get(this.baseUrl+'student/'+id, {headers: reqHeader});
  }

  deleteStudent(id: any): Observable<any>{
    const reqHeader = new HttpHeaders({'Content-Type': 'application/json', 'No-Auth': 'True'});
    return this.http.delete(this.baseUrl+'student/'+id, {headers: reqHeader});
  }

  createStudent(student: any) {
    const reqHeader = new HttpHeaders({'Content-Type': 'application/json', 'No-Auth': 'True'});
    return this.http.post(this.baseUrl+'student',student);
  }

  putStudent(student: any) {
    return this.http.put(this.baseUrl+'student/'+student.id, student);
  }

  changeStudent(student: any){
    return this.http.put(this.baseUrl+'student/status/'+student.id, student);
  }

}

