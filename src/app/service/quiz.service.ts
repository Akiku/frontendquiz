import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  // domain can be taken from env variable list
  private baseUrl = 'http://localhost:8081/api/';

  constructor(private http: HttpClient) { }


  getListQuiz(): Observable<any> {
    const reqHeader = new HttpHeaders({'Content-Type': 'application/json', 'No-Auth': 'True'});
    return this.http.get(this.baseUrl+'questions', {headers: reqHeader});
  }

  postListAnswerQuiz(s: any): Observable<any> {
    const reqHeader = new HttpHeaders({'Content-Type': 'application/json', 'No-Auth': 'True'});
    return this.http.post(this.baseUrl+'check/question',s);
  }

  createQuestion(object: any){
    const body: Object = {
      question: object.question,
      isAnswered: false,
      possibleAnswerList: object.possibleAnswerList
    };
    const reqHeader = new HttpHeaders({'Content-Type': 'application/json', 'No-Auth': 'True'});
    return this.http.post(this.baseUrl+'question',body, {headers: reqHeader});
  }


}
