import { Component, OnInit } from '@angular/core';
import {Form, FormArray, FormBuilder, FormGroup} from "@angular/forms";
import {QuizService} from "../../service/quiz.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.css']
})
export class QuestionsComponent implements OnInit {

  orderForm: any ;
  possibleAnswerList: Array<any> = [];
  allowSubmit = false;

  constructor(private formBuilder: FormBuilder, private quizService: QuizService,private toastr: ToastrService) { }

  ngOnInit(): void {
    this.orderForm = this.formBuilder.group({
      question: '',
      isAnswered: false,
      possibleAnswerList: this.formBuilder.array([ this.createItem() ])
    });
    this.resetForm()
  }


  createItem(): FormGroup {

    return this.formBuilder.group({
      name: '',
      correct: false
    });
  }

  addItem(): void {
    // @ts-ignore
    this.possibleAnswerList = this.orderForm.get('possibleAnswerList') as FormArray;
    this.possibleAnswerList.push(this.createItem());
  }

  resetForm(){
    this.orderForm.reset()
    const order = this.orderForm.get('possibleAnswerList') as FormArray;
    order.controls.forEach(order => order.patchValue({ name: "",correct: false}));
    console.log(this.orderForm.controls.possibleAnswerList.value[0].correct)
  }

  checkFormNull(){
    let name = this.orderForm.controls.possibleAnswerList.value[0].name
    let correct = this.orderForm.controls.possibleAnswerList.value[0].correct
    let question = this.orderForm.controls.question.value
    if(name != "" && question != ""){
      this.allowSubmit = true;
      console.log('You are allowed to submit')

    }else{
      this.orderForm.controls.possibleAnswerList.value[0].correct = false
      this.allowSubmit = false
      console.log('You are not allowed to submit', 'Name: ' + name, 'Question: ' + question, 'Correct: ' + correct )
    }
  }

  saveQuestion(){
    this.checkFormNull()
    if(this.allowSubmit){
      this.quizService.createQuestion(this.orderForm.value).subscribe((result: any) => {
        if (result.id > 0) {
          this.toastr.success('Intrebare creata cu success', 'Question');
          this.resetForm();
        }

      }, error => {
        console.log(error.error.message);
        this.toastr.error('Error on save: ' + error.error.message);
      });
    }
  }

  removeItem(index: any){
    if (this.possibleAnswerList.length <= 1) {
      return false;
    } else {
      this.orderForm.controls.possibleAnswerList.controls.splice(index, 1);
      return true;
    }
  }

}
