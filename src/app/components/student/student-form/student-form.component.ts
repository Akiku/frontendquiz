import {Component, OnInit} from '@angular/core';
import {StudentService} from '../../../service/student.service';
import {ToastrService} from 'ngx-toastr';
import {NgForm} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {

  student: any;
  id: any;

  constructor(private studentService: StudentService,
              private activeRoute: ActivatedRoute,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.student = this.activeRoute.params.subscribe(parametru => {
      this.id = +parametru['id'];
      if (this.id != null && this.id > 0) {
        this.getStudentInfo(this.id);
      }
    });
    this.resetForm();

  }

  resetForm(form?: NgForm) {
    if (form != null) {
      form.reset();
    }
    this.student = {
      id: null,
      active: null,
      name: null,
      surname: null,
      email: null,
      age: null,
      grade: null
    };
  }

  onSave(form: NgForm) {
    console.log(form)
    if (form.value.id == null) {
      this.insertRecord(form);
    } else {
      this.updateRecord(form);
    }
  }

  updateRecord(form: NgForm) {
    this.studentService.putStudent(form.value).subscribe((result) => {
      this.toastr.success('Student actualizat cu success', 'Actualizare');
      this.ngOnInit();
    });
  }


  insertRecord(form: NgForm) {
    console.log(form)
    if (form.value.name != null && form.value.surname != null && form.value.email != null) {
      this.studentService.createStudent(form.value).subscribe((result: any) => {
        if (result.id > 0) {
          this.toastr.success('Student creat cu success', 'Register');
          this.resetForm(form);
        }
      }, error => {
        console.log(error.error.message);
        this.toastr.error('Error on save: ' + error.error.message);
      });
    }else {
      this.toastr.warning('Please insert required data', 'Register');
    }
  }

  getStudentInfo(id: any) {
    this.studentService.getStudent(id).subscribe((result: any) => {
      this.student = result;
    });
  }


}
