import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {NgForm} from '@angular/forms';
import {TeacherService} from '../../../service/teacher.service';

@Component({
  selector: 'app-professor-form',
  templateUrl: './professor-form.component.html',
  styleUrls: ['./professor-form.component.css']
})
export class ProfessorFormComponent implements OnInit {

  professor: any;
  id: any;

  constructor(private professorService: TeacherService,
              private activeRoute: ActivatedRoute,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.professor = this.activeRoute.params.subscribe(parametru => {
      this.id = +parametru['id'];
      if (this.id != null && this.id > 0) {
        this.getProfessorInfo(this.id);
      }
    });
    this.resetForm();
  }

  resetForm(form?: NgForm) {
    if (form != null) {
      form.reset();
    }
    this.professor = {
      id: null,
      active: null,
      name: null,
      surname: null,
      email: null,
      age: null
    };
  }

  onSave(form: NgForm) {
    if (form.value.id == null) {
      this.insertRecord(form);
    } else {
      this.updateRecord(form);
    }
  }

  updateRecord(form: NgForm) {
    this.professorService.putProfessor(form.value).subscribe((result) => {
      this.toastr.success('Professor actualizat cu success', 'Actualizare');
      this.ngOnInit();
    });
  }


  insertRecord(form: NgForm) {
    if (form.value.name != null && form.value.surname != null && form.value.email != null) {
      this.professorService.createProfessor(form.value).subscribe((result: any) => {
        if (result.id > 0) {
          this.toastr.success('Professor creat cu success', 'Register');
          this.resetForm(form);
        }
      }, error => {
        console.log(error.error.message);
        this.toastr.error('Error on save: ' + error.error.message);
      });
    }else {
      this.toastr.warning('Please insert required data', 'Register');
    }
  }

  getProfessorInfo(id: any) {
    this.professorService.getProfessor(id).subscribe((result: any) => {
      this.professor = result;
    });
  }


}

