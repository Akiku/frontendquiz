import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {QuizService} from '../../../service/quiz.service';


interface quiz{
  id: any;
  question: any;
  selectedAnswers: any;
  isAnswered: any;
}


@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})


export class QuizComponent implements OnInit {

  quizArray: quiz[] = [];


  constructor(private quizService: QuizService,
    private toastr: ToastrService) {}

  ngOnInit() {
    this.getListOfQuestions();
  }

  getListOfQuestions() {

    this.quizService.getListQuiz().subscribe((s: any) => {
      this.quizArray = s;
    });
  }

}
