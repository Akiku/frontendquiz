import {Component, Input, OnInit} from '@angular/core';
import {QuizService} from '../../../service/quiz.service';

let dataCollect: any[][] = [];
let keepIdNameSelected: any[] =  [];
let keepSelectedName: string;

@Component({
  selector: 'question-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {



  @Input() quizArray: any;
  id: any;
  setQuestion =  0;
  finishQuestions = false;
  keepSelectedButton: any;
  correctAnswers = 0;
  answeredQuestions= 0;
  wrongAnswer = 0;
  getPercentage = 0;
  valueDash = 320;
  finalPerShow = 0;
  colorStatus = 'red';
  count = 0;
  interval=0;
  collectData = dataCollect;
  selectColorChange: any[] = []
  parentDOM: any
  index:any

  constructor(private quizService: QuizService) {}

  ngOnInit(): void {

  }

  getSelectedValue(ID: number,name: string,) {
    this.keepSelectedButton = ID;//Get id answer
    keepSelectedName = name; //Get name answer
    keepIdNameSelected.push({id:this.keepSelectedButton ,name: keepSelectedName})


    //Store id and name in this array
      localStorage.setItem(this.quizArray[this.setQuestion].id, JSON.stringify(keepSelectedName));

    this.quizArray[this.setQuestion].selectedAnswers = keepIdNameSelected ;//Store selected answers in SelectedAnswers
    this.selectColorChange.push({id:ID});
    console.log(this.selectColorChange);
    this.setColorSelected();
  }

  setColorSelected(){

  }

  //Loading count result
  counter() {
    this.count = 320;
    this.interval = this.interval = setInterval(() => {
      this.count = this.count === this.valueDash ? 0 : this.count - 1;
      if(this.count<=this.valueDash){
        clearInterval(this.interval)
      }
    }, 10);
  }

  //Load result
  checkResult(){

    if(this.answeredQuestions >= this.quizArray.length && this.finishQuestions){
      this.getPercentage= this.correctAnswers/this.quizArray.length;
      let value = Math.floor(this.valueDash*this.getPercentage);
      this.valueDash = 320 - value; // 320
      // console.log(this.valueDash);
      this.finalPerShow = (this.getPercentage*100);

      if (this.finalPerShow < 50){
        this.colorStatus='red';
      }else{
        this.colorStatus='green';
      }

      if(this.count<=this.valueDash){
        this.counter();
      }
    }
  }

  backQuestion() {
    if (this.setQuestion>=1){
      this.setQuestion--
    }
  }

  nextQuestion() {
    if (this.quizArray.length>this.setQuestion+1){
      this.setQuestion++

    }
  }

  // toate fucntiiile si medtodele sunt camelCase.
  // TO DO
  // sa fie adaptabil si pe inteles (adica postez un raspuns-liste = tradus o lista cu q/a)
  // incearca sa nu bagi 3 functii intrun click... si sa apelezi la metode ca sa nu iti creezi buguri.
  postAnswerList() {
    if(this.keepSelectedButton !=null){
      if(this.answeredQuestions != this.quizArray.length){
        this.quizService.postListAnswerQuiz(this.quizArray[this.setQuestion]).subscribe((result: any) => {
          this.checkAnswers(result);
        });
      }
    }
  }

  checkAnswers(result: any){
    console.log(this.quizArray)
    if(result.isCorrect){
      this.correctAnswers++
      this.answeredQuestions++
      result.isAnswered = true;
      if(this.answeredQuestions >= this.quizArray.length){this.finishQuestions = true; this.checkResult();}
    }else{
      this.wrongAnswer++
      this.answeredQuestions++
      result.isAnswered = true;
      if(this.answeredQuestions >= this.quizArray.length){this.finishQuestions = true; this.checkResult();}
    }
    // console.log("Result:",result)
    // console.log(this.finishQuestions)
  }

}


