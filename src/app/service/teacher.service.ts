import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {

  // domain can be taken from env variable list
  private baseUrl = 'http://localhost:8081/api/';

  constructor(private http: HttpClient) { }


  getListProfessor(): Observable<any> {
    const reqHeader = new HttpHeaders({'Content-Type': 'application/json', 'No-Auth': 'True'});
    return this.http.get(this.baseUrl+'teachers', {headers:reqHeader} );
  }

  getProfessor(id: any): Observable<any> {
    const reqHeader = new HttpHeaders({'Content-Type': 'application/json', 'No-Auth': 'True'});
    return this.http.get(this.baseUrl+'teachers/'+id, {headers: reqHeader});
  }

  deleteProfessor(id: any): Observable<any>{
    const reqHeader = new HttpHeaders({'Content-Type': 'application/json', 'No-Auth': 'True'});
    return this.http.delete(this.baseUrl+'teacher/'+id, {headers: reqHeader});
  }

  createProfessor(professor: any) {
    const reqHeader = new HttpHeaders({'Content-Type': 'application/json', 'No-Auth': 'True'});
    return this.http.post(this.baseUrl+'teacher',professor);
  }

  putProfessor(professor: any) {
    return this.http.put(this.baseUrl+'teacher/'+professor.id, professor);
  }

  changeProfessor(professor: any){
    return this.http.put(this.baseUrl+'teacher/status/'+professor.id, professor);
  }
}
