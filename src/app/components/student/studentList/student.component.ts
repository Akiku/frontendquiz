import { Component, OnInit } from '@angular/core';
import {StudentService} from '../../../service/student.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';



@Component({
  selector: 'app-student',
  templateUrl:'./student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  students: any[] = [];
  clickButton = true;
  arrowVerse = false;
  panelOpenState: boolean = false;

  constructor(private router: Router,
              private toastr: ToastrService,
              private studentService: StudentService) { }

  ngOnInit(): void {
   this.getListOfStudents();
    //Nu e necesar
    setInterval(() => {
      this.getListOfStudents();
    }, 35000);
  }


  // Normal shoudl be like this (no definition is public by default)
  // create a subscription

  private getListOfStudents() {
    this.studentService.getListStudent().subscribe((s: any) => {
      this.students = s;
    });
  }

  onRemoveStudent(id: any){
    this.studentService.deleteStudent(id).subscribe(() => {
      this.getListOfStudents();
    });
  }

  editStudent(id: any) {
    this.router.navigate(['student/form/edit/'+id]);
  }

  changeStatusStudent(student: any){
    this.studentService.changeStudent(student).subscribe(() => {
      if(student.active){
        this.toastr.success("Student activat", "Activation");
      }else {
        this.toastr.warning("Student deactivat", "Deactivation");
      }

      // this.ngOnInit();
    }, error => {
      this.toastr.error("Invalid Activation"+error.error.message);
    })
  }

  moveUp(){
    this.clickButton = false;
    this.arrowVerse = false;
    console.log("arrowVerse = false");
  }

  moveDown(){
    this.clickButton = false;
    this.arrowVerse = true;
    console.log("arrowVerse = true");
  }

  deactivateExpander(){
    this.clickButton=true;
  }
}
